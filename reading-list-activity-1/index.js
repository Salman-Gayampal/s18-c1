/*
Folder and Files Preparation: 

1. Create a "readingListAct" Folder in your Batch Folder. 
2. Create an index.html and index.js file. For the index.html title just write "Reading List Activity 1" 
3. Show your solutions for each problem in the index.js file. 
4. Once done, Create a remote repository named "readingListAct".
5. Save your repo link in Boodle: WDC028V1.5b-18-C1 | JavaScript - Function Parameters, Return Statement and Array Manipulations


*/


// Activity Template:
// (Copy this part on your template)


/*
    1.) Create a function that returns a passed string with letters in alphabetical order.
        Example string: 'mastermind'
        Expected output: 'adeimmnrst'
	
*/

// Code here:

//  Using a regular function
console.log("1.");
function passAlphabetically(myStr) {
    return myStr.split('').sort().join('');
}
console.log(passAlphabetically("Cucumber"));
// Cbcemruu

// Using arrow function

let alphabet_order = str => {
    return str.split('').sort().join('');
}
console.log(alphabet_order("Cucumber"));
// Cbcemruu

// or Implicit returning

const alphaOrder = someStr => someStr.split('').sort().join('');
console.log(alphaOrder("Cucumber"));
// Cbcemruu

/*
    2.) Write a simple JavaScript program to join all elements of the following array into a string.

    Sample array : myColor = ["Red", "Green", "White", "Black"];
        Expected output:
            Red,Green,White,Black
            Red,Green,White,Black
            Red+Green+White+Black

*/

// Code here:
console.log("2.");
someColor = ["Red", "Green", "White", "Black"];
console.log(someColor.toString()); 
// Using .toString() result: Red,Green,White,Black
console.log(someColor.join()); 
// Using .join() result: Red,Green,White,Black
console.log(someColor.join('+')); 
// Using .join() with a string "+" result: Red+Green+White+Black

/*
    3.) Write a function named birthdayGift that pass a gift as an argument.
        - if the gift is a stuffed toy, return a message that says: "Thank you for the stuffed toy, Michael!"
        - if the gift is a doll, return a message that says: "Thank you for the doll, Sarah!"
        - if the gift is a cake, return a message that says: "Thank you for the cake, Donna!"
        - if other gifts return a message that says: "Thank you for the (gift), Dad!"
        - create a global variable named myGift and invoke the function in the variable.
        - log the global var in the console

*/

// Code here:

console.log("3.");


let myGift = gift => {

    switch (gift) {
        case 'stuffed toy':
            console.log(`Thank you for the stuffed toy, Michael!`);
            break;

        case 'doll':
            console.log(`Thank you for the doll, Sarah!`);
            break;

        case 'cake':
            console.log(`Thank you for the cake, Donna!`);
            break;


        default:
            console.log(`Thank you for the ${gift}, Dad!`);
            break;
    }

}

myGift('stuffed toy'); // result: Thank you for the stuffed toy, Michael!
myGift('doll'); // result: Thank you for the doll, Sarah!
myGift('cake'); // result: TThank you for the cake, Donna!
myGift('Iphone 13'); // result: Thank you for the Iphone 13, Dad!


/*
    4.) Write a function that accepts a string as a parameter and counts the number of vowels within the string.

        Example string: 'The quick brown fox'
        Expected Output: 5

*/

// Code here:
console.log("4.");
function countVowel(str) {
    let vowel_list = 'aeiouAEIOU';
    let vcount = 0;

    for (let x = 0; x < str.length; x++) {
        if (vowel_list.indexOf(str[x]) !== -1) {
            vcount += 1;
        }

    }
    return vcount;
}
console.log(`The total number of vowel/s for the string is:`)
console.log(countVowel("The quick brown fox jumps over the lazy dog.")); // result: 11
